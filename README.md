# Diff Expressor  

### Description 
This is a command-line tool in R to perform differential expression. This program takes in a series of arguments for differential analysis.

### Dependencies
edgeR  
DESeq2  
limma  
optparse  
openxlsx  
limma  

### Input Files 
*Counts File*  
	1. The file is a tsv or csv  
	2. Low counts have been removed from the dataset  
	3. Each column represents a sample  
	4. The first column is the gene id (eg ENSMUSG, mgi_symbol, etc)  

*Covariate file*  
	1. The file is a tsv or csv  
	2. The first column must match the colnames of the counts file  

*gene meta file*  
	1. be a csv  
	2. the first column of the file must must be the same gene ID type as the counts file  

### Usage
```
Rscript diff_expressor.R \
	-c <counts <File> \
	-v <covariate <file> \
	--tool <edgeR, DESeq2, limma> \
	--condition1 <condition1 name for pairwise analysis> \
	--condition2 <condition2 name for pairwise analysis> \
	-d <design matrix> \
	-r <reduced matrix> \
	--pthresh <padj cutoff value> \
	--logFC <logFC cutoff> \
	--geneMeta <csv file containing gene meta info> \
	--bioMart <use bioMart to obtain gene meta info>
```

### Example Usage
#### Example1: edgeR-pairwise comparison

```
#!/usr/bin/env bash
~/diff_expressor.R \
        -c CleanCounts.csv \
        -v Covariates.csv \
        --tool edgeR \
        -a paired \
        --condition1 WT \
        --condition2 Mutant \
        --logFC 1.2 \
        -d ~0+factor+Sex \
        --geneMeta GeneMeta_Info/GeneMeta_Info.csv &> MouseSnack_edgeR_padj0.05_logFC1.2.log
```

#### Example2: DESeq2 ANOVA-like test

```
#!/usr/bin/env bash
~/diff_expressor.R \
        -c CleanCounts.csv \
        -v Covariates.csv \
        --tool DESeq2 \
        -a multi \
        -d ~0+factor+Sex \
        -r ~0+Sex \
        --geneMeta GeneMeta_Info/GeneMeta_Info.csv &> MouseSnack_DESeq2_padj0.05.log

```
### Output
The output is an excel file containing the results specified by the cutoff values.

### Out of Service  
edgeR ANOVA-like test  
limma pairwise comparision  
limma ANOVA-like test  
