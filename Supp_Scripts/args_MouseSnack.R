#!/usr/bin/env Rscript

#####################
# Libraries
#####################
cat("################\nLIBRARIES\n################\n")
cat(paste("Importing Libraries",Sys.time(),sep=" "),"\n\n")
shhhh <- suppressPackageStartupMessages
shhhh(library(optparse))
shhhh(library(openxlsx))
shhhh(library(biomaRt))
shhhh(library(DESeq2))
shhhh(library(edgeR))
shhhh(library(limma))

#####################
# Options
#####################
options.list <- list(
  make_option(c("-c","--count_file"),type="character",default=NULL,help="Count File:rows=genes;columns=sample_ID",metavar="character"),
  make_option(c("-v","--covariate_file"),type="character",default=NULL,help="Covariate File: rows=sample_ID; columns=metadata",metavar="character"),
  make_option("--tool",type="character",default=NULL,help="Tool for DE analysis; Options=edgeR,DESeq2,limma",metavar="character"),
  make_option(c("-a","--analysis_type"),type="character",default=NULL,help="Analysis type: options: paired, multi",metavar="character"),
  make_option("--condition1",type="character",default=NULL,help="Paired analysis only",metavar="character"),
  make_option("--condition2",type="character",default=NULL,help="Paired analysis only",metavar="character"),
  make_option(c("-d","--design"),type="character",default=NULL,help="Design matrix(No Spaces): Column name for variable of interest needs to be labeled 'factor'",metavar="character"),
  make_option(c("-r","--reduced"),type="character",default=NULL,help="Reduced Model for multi analysis (Not used in paired testing)"),
  make_option("--pthresh",type="numeric",default=0.05,help="padj Cutoff [default = 0.05]"),
  make_option("--logFC",type="numeric",default=0,help="log Fold Change Cutoff"),
  make_option("--geneMeta", type="character", default=NULL, help="Imports Gene Meta Information: rownames geneMeta must match rownames of count_file",metavar="character"),
  make_option("--biomart", type="character", default=NULL, help="Uses mouse bioMart to obtain mgi_gene symbol,chromosome,gene_start,gene_end. ID_types: 'ensembl_gene_id','ENTREZ','mgi_symbol'",metavar="character")
)

opt.parser <- OptionParser(option_list=options.list)
opt = parse_args(opt.parser)

if (is.null(opt$count_file)) {
  print_help(opt.parser)
  stop("At least one argument must be supplied (input file)", call.=FALSE)
}

cat("\n################\nSUPPLIED ARUGUMENTS\n################\n")
print(paste("Count file:",opt$count_file),sep="")
print(paste("Covariate file:",opt$covariate_file),sep="")
print(paste("Differential Expression Tool:",opt$tool))
print(paste("Analysis Type:",opt$analysis_type))
ifelse(!is.null(opt$condition1),print(paste("Condition1 (paired analysis only):",opt$condition1)),print("Condition1: No Argument Supplied"))
ifelse(!is.null(opt$condition2),print(paste("Condition2 (paired analysis only):",opt$condition2)),print("Condition2: No Argument Supplied"))
print(paste("Design Matrix:",opt$design))
ifelse(!is.null(opt$reduced),print(paste("Reduced Model (multi analysis only):",opt$reduced)),print("Reduced Model: No Argument Supplied"))
print(paste("P.adj cutoff:",opt$pthresh))
print(paste("logFC cutoff:",opt$logFC))